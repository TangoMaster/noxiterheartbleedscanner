package com.Noxiter.NoxiterHeartCheckApp;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ScanDeviceActivity extends Activity {
	private String app_labels[];
	private ArrayList<PackageInfoStruct> res = new ArrayList<PackageInfoStruct>();
	private ArrayList<PackageInfoStruct> vulnerablePacks = new ArrayList<PackageInfoStruct>();
	private ProgressDialog progDialog;
	private ProgressDialog progDialog2;
	private boolean systemOpenSSLVulernable = false;
	private ListView vulnerableListView;
	private TextView ListTitle;
	private TextView ListOpenSSLVersion;
	private VulnerabilityListAdapter adapter;
	private boolean badSSL;
	private boolean heartBeatToggle;
	private String OpenSSLVersion;
	private Integer switchDisplayPacks = -1;
	
	private ArrayList<PackageInfoStruct> getPackages() {
		ArrayList<PackageInfoStruct> apps = getInstalledApps(false);
		final int max = apps.size();
		for (int i = 0; i < max; i++) {
			apps.get(i);
		}
		return apps;
	}

	private ArrayList<PackageInfoStruct> getInstalledApps(boolean getSysPackages) {
		res.clear();
		List<PackageInfo> packs = getPackageManager().getInstalledPackages(0);
		try {
			app_labels = new String[packs.size()];
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), e.getMessage(),
					Toast.LENGTH_SHORT).show();
		}
		for (int i = 0; i < packs.size(); i++) {
			PackageInfo p = packs.get(i);
			if ((!getSysPackages) && (p.versionName == null)) {
				continue;
			}
			PackageInfoStruct newInfo = new PackageInfoStruct();
			newInfo.appname = p.applicationInfo.loadLabel(getPackageManager()).toString();
			newInfo.pname = p.packageName;
			newInfo.versionName = p.versionName;
			newInfo.versionCode = p.versionCode;
			newInfo.appInfo = p.applicationInfo;
			newInfo.shareObjectLocation = p.applicationInfo.nativeLibraryDir;
			newInfo.packageLocation = p.applicationInfo.sourceDir;
			res.add(newInfo);

			app_labels[i] = newInfo.appname;
		}
		return res;
	}
	
	private void CheckForBadOpenSSL(String filename) {
		badSSL = false;
		heartBeatToggle = false;

		final byte[] openSSLArray = "OpenSSL 1.0.1".getBytes();
		final byte[] HeartBeatOption = "dtls1_process_heartbeat".getBytes();

		File file = new File(filename);
		int size = (int) file.length();
		byte[] bytes = new byte[size];
		try {
			BufferedInputStream buf = new BufferedInputStream(
					new FileInputStream(file));
			buf.read(bytes, 0, bytes.length);
			buf.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		KMPMatch matcher = new KMPMatch();

		int opensslLocation = matcher.indexOf(bytes, openSSLArray);
	
		if (opensslLocation >= 0 && bytes[opensslLocation + 13] < 'g') {
			
			 badSSL = true;
			 byte[] ver = Arrays.copyOfRange(bytes, opensslLocation, opensslLocation + 14);
			 try {
				OpenSSLVersion = new String(ver, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			int heartBeatLocation = matcher.indexOf(bytes, HeartBeatOption);

			if (heartBeatLocation > 0)
				heartBeatToggle = true;
		}
	}
	
	private boolean unpackZip(String path, String zipname)
	{       
		Log.d("NoxHBScan", "UNPACKING FILE " + zipname);
		
	     InputStream is;
	     ZipInputStream zis;
	     try 
	     {
	         is = new FileInputStream(zipname);
	         zis = new ZipInputStream(new BufferedInputStream(is));          
	         ZipEntry ze;

	         while((ze = zis.getNextEntry()) != null) 
	         {
	             ByteArrayOutputStream baos = new ByteArrayOutputStream();
	             byte[] buffer = new byte[1024];
	             int count;

	             String filename = ze.getName();
	             
	             if ( ! filename.endsWith(".so") )
	             {
	            	 zis.closeEntry();
	            	 continue;
	             }
	             
	             Log.d("NoxHBScan", "WRITING FILE " + path + "/" + filename);
	             
				if (filename.lastIndexOf("/") >= 0 ) {
					File dirSure = new File(path + "/" + filename.substring(0, filename.lastIndexOf("/")));
					if (!dirSure.exists())
						dirSure.mkdirs();
				}
	             
	             FileOutputStream fout = new FileOutputStream(path + "/" + filename);

	             // reading and writing
	             while((count = zis.read(buffer)) != -1) 
	             {
	                 baos.write(buffer, 0, count);
	                 byte[] bytes = baos.toByteArray();
	                 fout.write(bytes);             
	                 baos.reset();
	             }

	             fout.close();               
	             zis.closeEntry();
	         }

	         zis.close();
	     } 
	     catch(IOException e)
	     {
	    	 Log.d("NoxHBScan", "EXCEPTION WRITING FILE ");
	         e.printStackTrace();
	         return false;
	     }

	    return true;
	}
	
	boolean ScanDirectory(String path)
	{
		File fileList[] = new File(path).listFiles();
		
		if ( fileList == null )
			return false;
		
		for ( int i = 0; i < fileList.length; i++ )
		{
			if ( fileList[i].isFile() && fileList[i].canRead() )
			{
				Log.d("NoxHBScan", "SCANNING FILE " + fileList[i].getAbsolutePath());
				CheckForBadOpenSSL(fileList[i].getAbsolutePath());
				if (badSSL == true && heartBeatToggle == false)
					return true;
			}
			else if ( fileList[i].isDirectory() && fileList[i].canRead() )
			{
				if ( ScanDirectory(fileList[i].getAbsolutePath()) )
					return true;
			}
		}
				
		return false;
	}
	
	void DeleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
	        String[] children = fileOrDirectory.list();
	        for (int i = 0; i < children.length; i++) {
	            File fox = new File(fileOrDirectory.getAbsolutePath() + "/" + children[i]);
	            if ( fox.isDirectory() )
	            	DeleteRecursive(fox);
	            fox.delete();
	        }
	    }
		fileOrDirectory.delete();
	}
	
	boolean GetAPKVulnerable(String apk)
	{
		ContextWrapper c = new ContextWrapper(this);
		String dataDir = c.getApplicationInfo().dataDir + "/app";
		File f = new File(dataDir);
	
		if ( ! f.exists() )
			if ( ! f.mkdir() )
				return false;
		
		try
		{
			unpackZip(dataDir, apk);
			boolean sup =  ScanDirectory(dataDir);
			DeleteRecursive(f);
			return sup;			
		}
		catch(Exception ex)
		{
		
		}
		
		return false;
	}
	

	void setProgressPercent(Integer percent) {
		progDialog.setProgress(percent);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_device);
		
		ArrayList<PackageInfoStruct> packs = new ArrayList<PackageInfoStruct>();
		
		progDialog = new ProgressDialog(this);
		progDialog2 = new ProgressDialog(this);

		vulnerableListView = (ListView) findViewById(R.id.vulerableListView);
		ListTitle = (TextView) findViewById(R.id.ListViewTitle);

		progDialog.setMessage("Finding Packages...\n");
		progDialog.setIndeterminate(false);
		progDialog.setCancelable(false);
		progDialog.show();

		new ScanDevice().execute(packs);
	}

	private void DisplayResults() {
		ListTitle.setText("HeartBleed Found in " + vulnerablePacks.size() + " Apps");
		adapter = new VulnerabilityListAdapter(this, vulnerablePacks);
		vulnerableListView.setAdapter(adapter);
		
		new AlertDialog.Builder(this)
	    .setTitle("Results Explanation	")
	    .setMessage("The Apps listed haved been found to contain a version of OpenSSL which is known to be vulnerable to the Heartbleed bug.  This does not mean they are definitively vulnerable, as they could have been compiled with the OPENSSL_NO_HEARTBEATS flag, which would disable this vulnerable feature.")
	    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            // continue with delete
	        }
	     })
	    .setIcon(android.R.drawable.ic_dialog_alert)
	     .show();
	}

	private class ScanDevice extends
			AsyncTask<ArrayList<PackageInfoStruct>, ArrayList<String>, Long> {

		private Integer progressPercent;

		protected void onProgressUpdate(ArrayList<String>... progress) {
			
			if ( switchDisplayPacks > 0 )
			{
				if ( progDialog.isShowing())
				{
					progDialog.hide();
					progDialog.dismiss();
				}
				else
				{
					progDialog2.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
					progDialog2.setMax(switchDisplayPacks);
					progDialog.setIndeterminate(false);
					progDialog.setCancelable(false);
					progDialog2.show();
				}
				synchronized (this) {
					progDialog2.setProgress(Integer.parseInt(progress[0].get(0)));
					progDialog2.setMessage(progress[0].get(1));
				}
			}
		}

		@Override
		protected void onPostExecute(Long result) {
			synchronized (this) {
				progDialog2.dismiss();
			}
			DisplayResults();
		}

		@Override
		protected Long doInBackground(ArrayList<PackageInfoStruct>... packsa) {
			progressPercent = 0;

			ArrayList<String> args = new ArrayList<String>();
			args.clear();
			args.add(progressPercent.toString());
			args.add("Finding Apps to Scan...");

			ArrayList<PackageInfoStruct> packs = getPackages();
			
			switchDisplayPacks = packs.size();
			
			vulnerablePacks.clear();
			systemOpenSSLVulernable = false;

			progressPercent = 1;
			args.add(progressPercent.toString());
			args.add("Scanning\n\"/system/lib/libssl.so\"...");
			publishProgress(args);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
			;

			CheckForBadOpenSSL("/system/lib/libssl.so");
			if (badSSL == true && heartBeatToggle == false)
				systemOpenSSLVulernable = true;

			PackageInfoStruct openSSL = new PackageInfoStruct();
			openSSL.appname = "System OpenSSL Library";
			openSSL.openSSLVersion = OpenSSLVersion;
			
			if ( systemOpenSSLVulernable )
			{
				openSSL.appname = "System OpenSSL: Vulnerable";
				openSSL.icon =  getResources().getDrawable( 0x0108001d ); // X icon
			}
			else
			{
				openSSL.appname = "System OpenSSL: Safe";
				openSSL.icon =  getResources().getDrawable( R.drawable.btn_check_buttonless_on ); // Check mark
			}
			vulnerablePacks.add(openSSL);
						
			for (int i = 0; i < packs.size(); i++) {
				synchronized (this) {
					progressPercent = i + 2;
					args.clear();
					args.add(progressPercent.toString());
					args.add("Scanning App\n \"" + packs.get(i).appname + "\"...");
					publishProgress(args);
				}

				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
				};
				
//				if ( GetAPKVulnerable(packs[0].get(i).packageLocation) )
//				{
//					vulnerablePacks.add(packs[0].get(i));
//				}

				if (packs.get(i).appLocation != null && packs.get(i).shareObjectLocation != null ) {
					if (ScanDirectory(packs.get(i).appLocation) || ScanDirectory(packs.get(i).shareObjectLocation)) {
						packs.get(i).openSSLVersion = OpenSSLVersion;
						packs.get(i).icon = packs.get(i).appInfo.loadIcon(getPackageManager());
						vulnerablePacks.add(packs.get(i));
					}
				}
		}

			return null;
		}
	}
}