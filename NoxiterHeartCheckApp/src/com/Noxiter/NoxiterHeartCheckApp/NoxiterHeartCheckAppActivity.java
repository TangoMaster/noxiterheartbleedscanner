package com.Noxiter.NoxiterHeartCheckApp;

import android.app.Activity;

public class NoxiterHeartCheckAppActivity extends Activity  {

	public native int IsHeartBleed(int argc, String[] argv);
	
    public int IsHeartBleedURL(String url)
    {
    	String[] theURLs = new String[2];
    	theURLs[0] = " ";
    	theURLs[1] = url;
    	
        int status = IsHeartBleed(2, theURLs);
        return status;
    }
	
	static {
		System.loadLibrary("heartleech");
	}	
}
