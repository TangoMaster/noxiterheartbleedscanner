package com.Noxiter.NoxiterHeartCheckApp;

import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;

public class PackageInfoStruct {
	public String appname = "";
	public String pname = "";
	public String versionName = "";
	public int versionCode = 0;
	public ApplicationInfo appInfo = null;
	public Drawable icon = null;
	public String shareObjectLocation = "";
	public String appLocation = "";
	public String packageLocation = "";
	public String openSSLVersion = "";
};
