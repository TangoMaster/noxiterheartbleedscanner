package com.Noxiter.NoxiterHeartCheckApp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.Noxiter.heartbleedbugcheck.util.SystemUiHider;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class Master extends Activity {
	
	final static String aboutTextString = "The Heartbleed bug in the OpenSSL project is a serious vulnerability which can be used to expose information such as private keys, passwords and other sensitive information.  Noxiter is a security software and consulting company with expertise in cryptography, application and network security. Noxiter is available for security audits, security software development or security training for your engineering department. Please tap on the banner above to visit our site.";
	
	public void AddButtonListeners()
	{
		final Button ScanDeviceButton = (Button) findViewById(R.id.ScanDeviceButton);
		final Button ScanURLButton = (Button) findViewById(R.id.ScanURLButton);
		final ImageView BannerButton = (ImageView) findViewById(R.id.bannerImage);
 
		ScanDeviceButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent ScanDeviceActivity = new Intent(getApplicationContext(), com.Noxiter.NoxiterHeartCheckApp.ScanDeviceActivity.class);
				startActivity(ScanDeviceActivity);
			}
		});
		
		ScanURLButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent ScanURLActivity = new Intent(getApplicationContext(), com.Noxiter.NoxiterHeartCheckApp.ScanURLActivity.class);
				startActivity(ScanURLActivity);
			}
		});
		
		BannerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.Noxiter.com"));
			    startActivity(browserIntent);
			}
		});
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_master);

		AddButtonListeners();
		
		TextView tv = (TextView) findViewById(R.id.aboutTextView);
		tv.append(aboutTextString);
	}
}

