package com.Noxiter.NoxiterHeartCheckApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.TextView;

public class ScanURLActivity extends Activity {

    private ProgressDialog progDialog;
	private TextView scanURLText;
	private TextView resultText;
	private Button   scanButton;
    private AlertDialog.Builder alert;
    private String url = "";
    private Integer result = -1;
    NoxiterHeartCheckAppActivity check;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_url);
		
		scanURLText = (TextView) findViewById(R.id.ScanURLText);
		scanButton = (Button) findViewById(R.id.ScanURLButton);
		resultText = (TextView) findViewById(R.id.resultText);
		progDialog = new ProgressDialog(this);
		alert = new AlertDialog.Builder(this);
		check = new NoxiterHeartCheckAppActivity();
		
		progDialog.setMessage("Scanning for Heartbleed...");
		progDialog.setIndeterminate(false);
		progDialog.setCancelable(false);
		
		scanButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				url = scanURLText.getText().toString();
				
				if ( ! URLUtil.isValidUrl(url) )
				{
					alert.setTitle("Invalid URL");
					alert.setMessage("The URL entered is not a valid URL.  Please try again.");
				    alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) { 
				            // continue with delete
				        }
				     })
				    .setIcon(android.R.drawable.ic_dialog_alert)
				    .show();
				    
				    return;
				}

				new ScanURL().execute(1);
			}
		});
	}
	
	private void DisplayResults()
	{
		progDialog.hide();
				
		resultText.setTextColor(Color.parseColor("#000000"));
		
		if ( result == 0 )
		{
			resultText.setTextColor(Color.parseColor("#66FF33"));
			resultText.setText(url + "\nis NOT Vulnerable to HeartBleed bug");
		}
		else if ( result == 1 )
		{
			resultText.setTextColor(Color.parseColor("#FF3300"));
			resultText.setText(url + "\nis VULERNABLE to HeartBleed bug");
		}
		else if ( result == 4 )
			resultText.setText("Unable to connect to\n" + url);
		else if ( result == 5 )
			resultText.setText("Unable to resolve host name\n" + url);
		else if ( result == 13 )
			resultText.setText("URL must be HTTPS\n" + url);
		else
			resultText.setText("General Error connecting to\n" + url);
	}
	
	private void DisplayProgress()
	{
		if ( ! progDialog.isShowing() )
			progDialog.show();
	}

	private class ScanURL extends AsyncTask<Integer, Integer, Integer> {
		@Override
		protected void onProgressUpdate(Integer ...progress) {
			DisplayProgress();
		}

		@Override
		protected void onPostExecute(Integer result) {
			DisplayResults();
		}

		@Override
		protected Integer doInBackground(Integer ...data) {
			publishProgress(1);
			result = check.IsHeartBleedURL(url);
		
			return result;
		}
	}
}
