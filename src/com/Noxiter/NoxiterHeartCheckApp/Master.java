package com.Noxiter.NoxiterHeartCheckApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.Noxiter.heartbleedbugcheck.util.SystemUiHider;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class Master extends Activity {
	
	final static String aboutTextString = "The Heartbleed bug in the OpenSSL project is a serious vulnerability which can be used to expose information such as private keys, passwords and other sensitive information.  Noxiter is a security software and consulting company with expertise in cryptography, application and network security. Noxiter is available for security audits, security software development or security training for your engineering department. Please tap on the banner above to visit our site.";
	
	public void AddButtonListeners()
	{
		final Button ScanDeviceButton = (Button) findViewById(R.id.ScanDeviceButton);
		final Button ScanURLButton = (Button) findViewById(R.id.ScanURLButton);
		final ImageView BannerButton = (ImageView) findViewById(R.id.bannerImage);
		final Button ContactNoxiter = (Button) findViewById(R.id.contactNoxiter);
 
		ScanDeviceButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent ScanDeviceActivity = new Intent(getApplicationContext(), com.Noxiter.NoxiterHeartCheckApp.ScanDeviceActivity.class);
				startActivity(ScanDeviceActivity);
			}
		});
		
		ScanURLButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent ScanURLActivity = new Intent(getApplicationContext(), com.Noxiter.NoxiterHeartCheckApp.ScanURLActivity.class);
				startActivity(ScanURLActivity);
			}
		});
		
		BannerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.Noxiter.com"));
			    startActivity(browserIntent);
			}
		});
		
		ContactNoxiter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "support@noxiter.com" });
				intent.putExtra(Intent.EXTRA_SUBJECT, "Information requested");
				intent.putExtra(Intent.EXTRA_TEXT, "");
				startActivity(Intent.createChooser(intent, "Send Email"));
			}
		});
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_master);

		AddButtonListeners();
		
		Intent i= new Intent(this, NoxiterHBService.class);
		startService(i);
		
		TextView tv = (TextView) findViewById(R.id.aboutTextView);
		tv.append(aboutTextString);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);

		return true;
	}

	public static String readFileAsString(String filePath) {

		String result = "";
		File file = new File(filePath);
		if (file.exists()) {
			FileInputStream fis = null;
			try {

				fis = new FileInputStream(file);
				char current;
				while (fis.available() > 0) {
					current = (char) fis.read();
					result = result + String.valueOf(current);

				}
		} catch (Exception e) {
			
			} finally {
				if (fis != null)
					try {
						fis.close();
					} catch (IOException ignored) {
					}
			}
		}
		return result;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.eula) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Noxiter Heartbleed Scanner EULA");

			AssetManager assetMgr = this.getAssets();
			String EulaText = "";
			InputStream is;
			try {
				is = assetMgr.open("eula.txt");
				 byte[] input = new byte[is.available()];
				 while (is.read(input) != -1) {}
				 EulaText += new String(input);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.eula_dialog, null);

			TextView text = (TextView) layout.findViewById(R.id.EulaText);
			text.setMovementMethod(LinkMovementMethod.getInstance());
			text.setText(Html.fromHtml(EulaText));
			builder.setView(layout);
			builder.show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

