package com.Noxiter.NoxiterHeartCheckApp;

import android.app.Activity;

public class NoxiterHeartCheckAppActivity extends Activity  {

	public native int IsHeartBleed(int argc, String[] argv);
	
    public int IsHeartBleedURL(String hostname, Integer port)
    {
    	String[] theURLs = new String[3];
    	theURLs[0] = " ";
    	theURLs[1] = hostname;
    	theURLs[2] = "-p" + port;
    	
        int status = IsHeartBleed(3, theURLs);
        return status;
    }
	
	static {
		System.loadLibrary("bleedtest");
	}	
}
