package com.Noxiter.NoxiterHeartCheckApp;

import java.net.URI;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.TextView;

public class ScanURLActivity extends Activity {

    private ProgressDialog progDialog;
	private TextView scanURLText;
	private TextView resultText;
	private Button   scanButton;
    private AlertDialog.Builder alert;
    private String url = "";
    private Integer result = -1;
    private URI theURI;
    NoxiterHeartCheckAppActivity check;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_url);
		
		scanURLText = (TextView) findViewById(R.id.ScanURLText);
		scanButton = (Button) findViewById(R.id.ScanURLButton);
		resultText = (TextView) findViewById(R.id.resultText);
		progDialog = null;
			
		check = new NoxiterHeartCheckAppActivity();
		alert = new AlertDialog.Builder(this);
		
		scanButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				url = scanURLText.getText().toString();

				if ( ! URLUtil.isValidUrl(url) || ! URLUtil.isHttpsUrl(url) )
				{
					alert.setTitle("Invalid URL");
					alert.setMessage("The URL entered must be valid and start with https:// ...  Please try again.");
				    alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) { 
				            // continue with delete
				        }
				     })
				    .setIcon(android.R.drawable.ic_dialog_alert)
				    .show();
				    
				    return;
				}
				
				try{
				theURI = new URI(url);
				}catch(Exception e)
				{
				}
				
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(scanURLText.getWindowToken(), 0);
				
				new ScanURL().execute(1);
			}
		});
	}
	
	private void DisplayResults()
	{
		progDialog.hide();
		progDialog.dismiss();
		progDialog = null;
				
		resultText.setTextColor(Color.parseColor("#C0C0C0"));
		
		if ( result == 0 )
		{
			resultText.setTextColor(Color.parseColor("#66FF33"));
			resultText.setText(url + "\nis NOT Vulnerable to HeartBleed bug");
		}
		else if ( result == 1 )
		{
			resultText.setTextColor(Color.parseColor("#FF3300"));
			resultText.setText(url + "\nis VULNERABLE to HeartBleed bug");
		}
		else if ( result == 4 )
			resultText.setText("Unable to connect to\n" + url);
		else if ( result == 5 )
			resultText.setText("Unable to resolve host name\n" + url);
		else if ( result == 13 )
			resultText.setText("URL must be HTTPS\n" + url);
		else
			resultText.setText("General Error connecting to\n" + url);
	}
	
	private void DisplayProgress()
	{
		if ( progDialog == null )
		{
			progDialog = new ProgressDialog(this);
			progDialog.setMessage("Scanning for Heartbleed...");
			progDialog.setIndeterminate(false);
			progDialog.setCancelable(false);	
			progDialog.show();
		}
	}

	private class ScanURL extends AsyncTask<Integer, Integer, Integer> {
		@Override
		protected void onProgressUpdate(Integer ...progress) {
			DisplayProgress();
		}

		@Override
		protected void onPostExecute(Integer result) {
			DisplayResults();
		}

		@Override
		protected Integer doInBackground(Integer ...data) {
			publishProgress(1);
			int port = 443;
			
			if ( theURI.getPort() != -1 )
				port = theURI.getPort();
			
			result = check.IsHeartBleedURL(theURI.getHost(), port);
		
			return result;
		}
	}
}
