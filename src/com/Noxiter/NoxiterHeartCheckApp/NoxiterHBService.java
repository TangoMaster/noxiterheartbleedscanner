package com.Noxiter.NoxiterHeartCheckApp;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class NoxiterHBService extends Service {
	private InstalledNotificationReceiver br;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO do something useful

		br = new InstalledNotificationReceiver();

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
		intentFilter.addDataScheme("package");
		registerReceiver(br, intentFilter);

		return Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO for communication return IBinder implementation
		return null;
	}
}