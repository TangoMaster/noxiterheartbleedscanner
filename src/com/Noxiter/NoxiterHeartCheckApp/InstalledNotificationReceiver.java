package com.Noxiter.NoxiterHeartCheckApp;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.v4.app.NotificationCompat;

public class InstalledNotificationReceiver extends BroadcastReceiver {

	private ScanDeviceActivity sda;
	private int notificationID;

	@Override
	public void onReceive(Context context, Intent intent) {

		String action = intent.getAction();
		
		if ( action.compareToIgnoreCase(Intent.ACTION_BOOT_COMPLETED) == 0 )
		{
			Intent startServiceIntent = new Intent(context, NoxiterHBService.class);
	        context.startService(startServiceIntent);
	        return;
		}
		
		PackageManager pm = context.getPackageManager();
		ApplicationInfo ai = null;
		try {
			ai = pm.getApplicationInfo(
					intent.getData().getSchemeSpecificPart(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (ai == null)
			return;

		String appName = ai.loadLabel(pm).toString();

		notificationID = 31337;
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context);

		mBuilder.setSmallIcon(R.drawable.ic_launcher);
		mBuilder.setContentTitle("Noxiter Heartbleed Scanner");
		mBuilder.setContentText("Scanning " + appName);
		mBuilder.setTicker("Scanning " + appName);

		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.notify(notificationID, mBuilder.build());

		sda = new ScanDeviceActivity();

		if (ai.nativeLibraryDir != null) {
			if (sda.ScanDirectory(ai.nativeLibraryDir)) {
				if (sda.heartBeatExists) {
					mBuilder.setContentText("VULNERABLE : " + appName);
					mBuilder.setTicker("VULNERABLE : " + appName);
				} else {
					mBuilder.setContentText("Safe : " + appName);
					mBuilder.setTicker("Safe : " + appName);
				}
				mNotificationManager.notify(notificationID, mBuilder.build());
			} else {
				mBuilder.setContentText("Safe : " + appName);
				mBuilder.setTicker("Safe : " + appName);
				mNotificationManager.notify(notificationID, mBuilder.build());
			}
		} else {
			mBuilder.setContentText("Safe : " + appName);
			mBuilder.setTicker("Safe : " + appName);
			mNotificationManager.notify(notificationID, mBuilder.build());
		}
	}
}